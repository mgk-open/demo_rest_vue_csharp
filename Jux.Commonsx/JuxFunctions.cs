﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Data.Common;
using System.IO;
/// <summary>
/// agustinistmo@gmail.com
/// </summary>
namespace Mgk.Commonsx
{
    public class MgkFunctions
    {
        public static string WORK_ENVIRONMENT = null;

        public static string MD5Hash(string input)
        {
            using (var md5 = MD5.Create())
            {
                var result = md5.ComputeHash(Encoding.ASCII.GetBytes(input));
                return Encoding.ASCII.GetString(result);
            }
        }

        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public static string Md5i2(string str)
        {
            string md5i = Md5(str);
            md5i = Md5(Reverse(md5i));
            return Reverse(md5i);
        }

        public static string Md5(string str)
        {
            MgkStaticMessage.Clear();
            try
            {
                MD5 md5 = MD5CryptoServiceProvider.Create();
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] stream = null;
                StringBuilder sb = new StringBuilder();
                stream = md5.ComputeHash(encoding.GetBytes(str));
                for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
                return sb.ToString();
            }
            catch (Exception e)
            {
                MgkLog.Warning(new MgkMessage
                {
                    Source = "MgkFunctions.MD5",
                    Exception = e.ToString(),
                    OData = new { str = str }
                });
                return "";
            }
        }

        public bool EsNumeroEntero(String Cadena)
        {
            Regex patronNumerico = new Regex("[^0-9]");
            return !patronNumerico.IsMatch(Cadena);
        }

        public static bool EsNumeroFlotante(String Cadena)
        {
            Regex patronNumerico = new Regex(@"^(\d|-)?(\d|,)*\.?\d*$");
            return patronNumerico.IsMatch(Cadena);
        }

        public bool EsAlfabetico(String Cadena)
        {
            Regex patronAlfabetico = new Regex("[^a-zA-Z]");
            return !patronAlfabetico.IsMatch(Cadena);
        }

        public bool EsAlfanumerico(String Cadena)
        {
            Regex patronAlfanumerico = new Regex("[^a-zA-Z0-9]");
            return !patronAlfanumerico.IsMatch(Cadena);
        }

        /// <summary>
        /// Convertir Texto a Entero
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static int StrToInt(String strVal, int defaultValor = 0)
        {
            MgkStaticMessage.Clear();
            try
            {
                if (strVal != null && strVal != "")
                    defaultValor = Int32.Parse(strVal);
            }
            catch (Exception e)
            {
                MgkLog.Warning(new MgkMessage
                {
                    Number = -120000,
                    Code = "EX-WAR",
                    Source = "MgkFunctions.StrToInt",
                    Exception = e.ToString(),
                    OData = new
                    {
                        strVal = strVal
                    }
                });
            }
            return defaultValor;
        }

        /// <summary>
        /// Convierte un straing a un valor decimal
        /// </summary>
        /// <param name="strInt">Valor a convertir</param>
        /// <param name="name">Nombre de la variable origen para rastraear error</param>
        /// <param name="defaultValue">Valor en caso de error</param>
        /// <returns></returns>
        public static Decimal StrToDecimal(String strVal, String name = "", Decimal defaultValue = 0)
        {
            MgkStaticMessage.Clear();
            try
            {
                if (strVal != null && strVal != "")
                    defaultValue = (Decimal)Convert.ToDecimal(strVal);
            }
            catch (Exception e)
            {
                MgkLog.Warning(new MgkMessage
                {
                    Number = -120000,
                    Code = "EX-WAR",
                    Source = "MgkFunctions.StrToDecimal",
                    Exception = e.ToString(),
                    OData = new
                    {
                        strVal = strVal,
                        name = name
                    }
                });
                defaultValue = 0;
            }
            return defaultValue;
        }

        public static Double StrToDouble(String strVal, String name = "", Double defaultValue = 0)
        {
            MgkStaticMessage.Clear();
            try
            {
                if (strVal != null && strVal != "")
                    defaultValue = (Double)Convert.ToDouble(strVal);
            }
            catch (Exception e)
            {
                MgkLog.Warning(new MgkMessage
                {
                    Number = -120000,
                    Code = "EX-WAR",
                    Source = "MgkFunctions.StrToDouble",
                    Exception = e.ToString(),
                    OData = new
                    {
                        strVal = strVal,
                        name = name
                    }
                });
                defaultValue = 0;
            }
            return defaultValue;
        }

        public static bool BStrToDateTime(DateTime dateTime, string sDateTime)
        {
            MgkStaticMessage.Clear();
            try
            {
                dateTime = DateTime.Parse(sDateTime);
                return true;
            }
            catch (Exception e)
            {
                MgkLog.Warning(new MgkMessage
                {
                    Number = -120000,
                    Code = "EX-WAR",
                    Source = "MgkFunctions.bStrToDateTime",
                    Exception = e.ToString(),
                    OData = new
                    {
                        dateTime = dateTime,
                        sDateTime = sDateTime

                    }
                });
                return false;
            }
        }

        public static DateTime StrToDateTime(string sDateTime)
        {
            MgkStaticMessage.Clear();
            try
            {
                return DateTime.Parse(sDateTime);
            }
            catch (Exception e)
            {
                MgkLog.Warning(new MgkMessage
                {
                    Number = -120000,
                    Code = "EX-WAR",
                    Source = "MgkFunctions.StrToDateTime",
                    Exception = e.ToString(),
                    OData = new
                    {
                        sDateTime = sDateTime
                    }
                });
                return new DateTime();
            }
        }

        public static Boolean StrToBoolean(String strBool, bool vnull)
        {
            return strBool == null ? vnull : StrToBoolean(strBool);
        }

        public static Boolean StrToBoolean(String strBool)
        {
            if (strBool == null)
                return false;
            return strBool.ToUpper().Equals("TRUE") || strBool.ToUpper().Equals("1");
        }

        public static float StrToFloat(String strVal, String name = "", float retval = 0)
        {
            MgkStaticMessage.Clear();
            try
            {
                if (strVal != null && strVal != "")
                    retval = (float)Convert.ToDouble(strVal);
            }
            catch (Exception e)
            {
                MgkLog.Warning(new MgkMessage
                {
                    Number = -120000,
                    Code = "EX-WAR",
                    Source = "MgkFunctions.StrToFloat",
                    Exception = e.ToString(),
                    OData = new
                    {
                        strVal = strVal,
                        name = name
                    }
                });
                retval = 0;
            }
            return retval;
        }

        public static string Base64DeCode(string base64EnCodedData)
        {
            var base64EnCodedBytes = System.Convert.FromBase64String(base64EnCodedData);
            return System.Text.Encoding.UTF8.GetString(base64EnCodedBytes);
        }

        public static string Base64EnCode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        /// <summary>
        /// Leer nombre de ambiente de trabajo
        /// </summary>
        public static string ReadWorkEnvironment()
        {
            if (MgkFunctions.WORK_ENVIRONMENT == null)
            {
                MgkFunctions.WORK_ENVIRONMENT = ConfigurationManager.AppSettings["Mgk.workEnvironment"];
                if (MgkFunctions.WORK_ENVIRONMENT != null && MgkFunctions.WORK_ENVIRONMENT.Trim() != "")
                    MgkFunctions.WORK_ENVIRONMENT = MgkFunctions.WORK_ENVIRONMENT.Trim() + ".";
            }
            if (MgkFunctions.WORK_ENVIRONMENT == null)
                MgkFunctions.WORK_ENVIRONMENT = "";
            return MgkFunctions.WORK_ENVIRONMENT;
        }


        /// <summary>
        /// Obtener valor en archivo de configuraciones
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="noexiste"></param>
        /// <param name="useWorkEnvironment">Usar variables por ambiente de trabajo</param>
        /// <returns></returns>
        public static string AppSettings(string nombre, string noexiste = null, bool useWorkEnvironment = false)
        {
            MgkStaticMessage.Clear();
            string valor = null;
            try
            {
                if (useWorkEnvironment)
                    nombre = ReadWorkEnvironment() + nombre;
                valor = ConfigurationManager.AppSettings[nombre];
                if (valor == null)
                    valor = noexiste;
            }
            catch (Exception e)
            {
                MgkLog.Warning(new MgkMessage
                {
                    Number = -120000,
                    Code = "EX-WAR",
                    Source = "MgkFunctions.AppSettings",
                    Exception = e.ToString(),
                    OData = new
                    {
                        nombre = nombre,
                        noexiste = noexiste
                    }
                });
                valor = noexiste;
            }
            return valor;
        }

        public static object GetFromDicctionary(object dictionary, string key, object notFounded = null)
        {
            MgkStaticMessage.Clear();
            try
            {
                Dictionary<string, object> dic = (Dictionary<string, object>)dictionary;
                if (dic.ContainsKey(key))
                    return dic[key];
            }
            catch (Exception ex)
            {
                MgkStaticMessage.Message.SetMessage(new MgkMessage
                {
                    Number = -120000,
                    Code = "EX-WAR",
                    Source = "MgkFunctions.GetFromDicctionary",
                    Exception = ex.ToString(),
                    OData = new
                    {
                        dictionary = dictionary,
                        key = key,
                        notFounded = notFounded
                    }
                });
                MgkLog.Warning(MgkStaticMessage.Message);
            }
            return notFounded;
        }

        public static Dictionary<string, object> ObjectToDicctionary(Object oObject)
        {
            MgkStaticMessage.Clear();
            try
            {
                string txt = Newtonsoft.Json.JsonConvert.SerializeObject(oObject);
                Dictionary<string, object> dicx = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(txt);
                return dicx;
            }
            catch (Exception e)
            {
                MgkLog.Error(new MgkMessage
                {
                    Number = -100000,
                    Message = "Error al convertir Objeto en Diccionario",
                    Exception = e.ToString(),
                    OData = new
                    {
                        oObject = oObject
                    }
                });
            }
            return null;
        }


        public static string StrRequestFromObject(Object oObject)
        {
            MgkStaticMessage.Clear();
            StringBuilder strRequest = new StringBuilder("");
            try
            {
                Dictionary<string, object> dictio = MgkFunctions.ObjectToDicctionary(oObject);
                int i = 1;
                foreach (var item in dictio)
                {
                    if (item.Value == null)
                        continue;
                    if (item.Value is DateTime)
                        strRequest.Append(string.Format("{0}={1}", item.Key, ((DateTime)item.Value).ToString("yyyy-MM-dd")));
                    else
                        strRequest.Append(string.Format("{0}={1}", item.Key, item.Value.ToString()));
                    if (i++ < dictio.Count)
                        strRequest.Append("&");
                }
            }
            catch (Exception e)
            {
                MgkLog.Error(new MgkMessage
                {
                    Number = -100000,
                    Message = "Error al convertir Objeto stringRequest",
                    Exception = e.ToString(),
                    Messagex = "strRequestFromObject",
                    OData = new
                    {
                        oObject = oObject
                    }
                });
            }
            return strRequest.ToString();
        }

        public static byte[] GetFile(string filePath)
        {
            MgkStaticMessage.Clear();
            try
            {
                System.IO.FileStream fs = System.IO.File.OpenRead(filePath);
                byte[] data = new byte[fs.Length];
                int br = fs.Read(data, 0, data.Length);
                if (br != fs.Length)
                    throw new System.IO.IOException(filePath);
                fs.Close();
                return data;
            }
            catch (Exception e)
            {
                MgkLog.Error(new MgkMessage
                {
                    Number = -100000,
                    Message = "Error al obtener bytes del archivo",
                    Exception = e.ToString(),
                    Messagex = "GetFile",
                    OData = new
                    {
                        filePath = filePath
                    }
                });
            }
            return null;
        }

        public static String ColumnToString(DbDataReader oDbDataReader, String nombreColumna)
        {
            String sRetVal;

            if (!oDbDataReader.IsDBNull(oDbDataReader.GetOrdinal(nombreColumna)))
                sRetVal = oDbDataReader[nombreColumna].ToString();
            else
                sRetVal = String.Empty; 

            return sRetVal;
        }

        public static DateTime ColumnToDateTime(DbDataReader oDbDataReader, String nombreColumna,DateTime Fecha)
        {
            DateTime sRetVal= Fecha;

            if (!oDbDataReader.IsDBNull(oDbDataReader.GetOrdinal(nombreColumna)))
                sRetVal = StrToDateTime(oDbDataReader[nombreColumna].ToString());

            return sRetVal;
        }

        public static int ColumnToInt(DbDataReader oDbDataReader, String nombreColumna)
        {
            int iRetVal;

            if (!oDbDataReader.IsDBNull(oDbDataReader.GetOrdinal(nombreColumna)))
                iRetVal = Convert.ToInt32(oDbDataReader[nombreColumna]);
            else
                iRetVal = 0;

            return iRetVal;
        }

        public static decimal ColumnToDecimal(DbDataReader oDbDataReader, String nombreColumna)
        {
            decimal iRetVal;

            if (!oDbDataReader.IsDBNull(oDbDataReader.GetOrdinal(nombreColumna)))
                iRetVal = Convert.ToDecimal(oDbDataReader[nombreColumna]);
            else
                iRetVal = 0;

            return iRetVal;
        }

        public static Double ColumnToDouble(DbDataReader oDbDataReader, String nombreColumna)
        {
            Double iRetVal;

            if (!oDbDataReader.IsDBNull(oDbDataReader.GetOrdinal(nombreColumna)))
                iRetVal = Convert.ToDouble(oDbDataReader[nombreColumna]);
            else
                iRetVal = 0;

            return iRetVal;
        }

        public static String[] GetFiles(string path)
        {
            String[] files = null;

            if (Directory.Exists(path))
            {
                // This path is a directory
                files = Directory.GetFiles(path);
            }
            else
            {
                Console.WriteLine("{0} is not a valid file or directory.", path);
            }

            return files;
        }

    }
}
