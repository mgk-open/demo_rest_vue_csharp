﻿using Mgk.Commonsx;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

/// <summary>
/// agustinistmo@gmail.com
/// </summary>
namespace Mgk.Commonsx
{
    public class MgkLog
    {
        public enum Type { LOG, DEBUG, WARNING,ERROR };
        public static String LOG_DIRECTORY = "0";
        public static String LOG_SEPARATOR = "\t";
        public static String LOG_FORMAT_NAME_DATETIME = "";
        private static string prefixName="";       

        public void Agregar(String Source, String Message, Type Yype)
        {
        }

        public static void Log(String Source, String Message)
        {
            Write(Message, Type.LOG.ToString());
        }

        public static void Debug(String Source, String Message)
        {
            Write(Message, Type.DEBUG.ToString());
        }

        public static void Error(String Source, String Message)
        {
            Write(Message, Type.ERROR.ToString());
        }

        public static void Error(MgkMessage MgkMessage)
        {
            Write(MgkMessage.ToJson(), Type.ERROR.ToString());
        }

        public static void Warning(String Source, String Message)
        {
            Write(Message, Type.WARNING.ToString());
        }

        public static void Log(MgkMessage Message)
        {
            Write(Message.ToJson(), Type.LOG.ToString());
        }

        public static void Debug(MgkMessage Message)
        {
            Write(Message.ToJson(), Type.DEBUG.ToString());
        }

        public static void Warning(MgkMessage Message, bool bWrite=false)
        {
            Write(Message.ToJson(), Type.WARNING.ToString());
            if (bWrite)
                Message.WriteLine();
        }

        private static void ReadLogDirectory()
        {
            MgkFunctions.ReadWorkEnvironment();

            if (LOG_DIRECTORY != "0")
                return;
            try
            {
                LOG_FORMAT_NAME_DATETIME = ConfigurationManager.AppSettings[MgkFunctions.WORK_ENVIRONMENT +"Mgk.LogFormatNameDateTime"];
                LOG_FORMAT_NAME_DATETIME = LOG_FORMAT_NAME_DATETIME ?? "";
                if (ConfigurationManager.AppSettings[MgkFunctions.WORK_ENVIRONMENT +"Mgk.LogDirectory"] == null)
                {
                    Console.WriteLine("--- MyLog.leerLogDirectory(): Algo paso con la lectura de LOG_DIRECTORY");
                    LOG_DIRECTORY = "1";
                }
                else
                {
                    LOG_DIRECTORY = ConfigurationManager.AppSettings[MgkFunctions.WORK_ENVIRONMENT + "Mgk.LogDirectory"];
                    LOG_SEPARATOR = MgkFunctions.AppSettings(MgkFunctions.WORK_ENVIRONMENT + "Mgk.LogSeparator", LOG_SEPARATOR);
                    if (!Directory.Exists(LOG_DIRECTORY))
                        Directory.CreateDirectory(LOG_DIRECTORY);
                }
            }
            catch (Exception )
            {
                LOG_DIRECTORY = "";
            }
        }

        /// <summary>
        /// Escribir en archivo
        /// </summary>
        /// <param name="line"></param>
        /// <param name="tipo"></param>
        public static void Write(String line, String type)
        {
            ReadLogDirectory();
            if (LOG_DIRECTORY.Equals("1") || LOG_DIRECTORY.Equals("2"))
                return;

            try
            {
                Thread thread = new Thread(() => {
                    try
                    {
                        if (LOG_FORMAT_NAME_DATETIME == "")
                            prefixName = "";
                        else
                            prefixName = DateTime.Now.ToString(LOG_FORMAT_NAME_DATETIME) + "_";
                        String fileName = LOG_DIRECTORY + prefixName +
                            (type.Equals("") ? "" : type + "_") +
                            ".txt";
                        StreamWriter sw = new StreamWriter(fileName, true);
                        sw.WriteLine(DateTime.Now.ToString("s") + "\t[" + type + "]" + LOG_SEPARATOR + line);
                        sw.Close();
                    }catch(Exception ee)
                    {

                    }
                });
                thread.Start();
            }
            catch (Exception )
            {
                //DIRECTORY = "2";
            }
        }
    }
}
