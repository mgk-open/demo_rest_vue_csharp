﻿/// <summary>
/// agustinistmo@gmail.com
/// </summary>
namespace Mgk.Commonsx
{
    public static class MgkStaticMessage
    {
        public static MgkMessage Message { get; set; }= new MgkMessage();
        public static void Clear()
        {
            Message.Clear();
        }
    }
}