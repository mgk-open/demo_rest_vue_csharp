﻿using Mgk.Demo.Resvue.Base.Ticket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Jux.WebAppRestVue.Controllers.REST
{
    public class TicketsController : ApiController
    {
        // GET: api/Tickets
        public IEnumerable<TicketModel> Get()
        {
            return (new TicketDao()).Listado();
            //List<TicketModel> Ticktes = new List<TicketModel>();
            //Ticktes.Add(new TicketModel { Ticket_id = 5, Asunto = "Error de dedo", Descripcion = "El usuario presiono una tecla por error" });
            //Ticktes.Add(new TicketModel { Ticket_id = 6, Asunto = "Dato no existe", Descripcion = "No se ha encontrado la informacion solicitada" });
            //Ticktes.Add(new TicketModel { Ticket_id = 16, Asunto = "El oso panda", Descripcion = "y el osito dormilon" });
            //return Ticktes;
        }

        // GET: api/Tickets/5
        public TicketModel Get(int id)
        {
            return (new TicketDao()).Leer(new TicketModel { Ticket_id=id});

            //List<TicketModel> Ticktes = new List<TicketModel>();
            //Ticktes.Add(new TicketModel { Ticket_id = 5, Asunto = "Error de dedo", Descripcion = "El usuario presiono una tecla por error" });
            //Ticktes.Add(new TicketModel { Ticket_id = 6, Asunto = "Dato no existe", Descripcion = "No se ha encontrado la informacion solicitada" });
            //Ticktes.Add(new TicketModel { Ticket_id = 16, Asunto = "El oso panda", Descripcion = "y el osito dormilon" });
            //if (Ticktes.Exists(x => x.Ticket_id == id))
            //    return Ticktes.Find(x => x.Ticket_id == id);
            //else
            //    return null;
        }

        // POST: api/Tickets
        public TicketModel Post([FromBody]TicketModel value)
        {
            return (new TicketDao()).Agregar(value);

            //value.Ticket_id = DateTime.Now.Millisecond;
            //return value;
        }

        // PUT: api/Tickets/5
        public TicketModel Put(int id, [FromBody]TicketModel value)
        {
            return (new TicketDao()).Modificar(value);

        }

        // DELETE: api/Tickets/5
        public int Delete(int id)
        {
            return (new TicketDao()).Eliminar(new TicketModel { Ticket_id = id });
        }
    }
}
