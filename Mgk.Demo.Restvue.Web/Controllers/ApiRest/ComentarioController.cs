﻿using Mgk.Commonsx;
using Mgk.Demo.Restvue.Base.Comentario;
using Mgk.Demo.Restvue.Web.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Mgk.Demo.Restvue.Web.Controllers.ApiRest
{
    [FiltroAccesoApi]
    public class ComentarioController : ApiController
    {
        // GET: api/Comentario
        public async Task<MgkMessage> Get()
        {
            ComentarioCtrl ComentarioC = new ComentarioCtrl();
            var Items = ComentarioC.GetAll();
            ComentarioC.Message.OData = Items;
            return ComentarioC.Message;
        }

        // GET: api/Comentario/5
        public async Task<MgkMessage> Get(int id)
        {
            ComentarioCtrl ComentarioC = new ComentarioCtrl();
            ComentarioC.Message.OData = ComentarioC.Get(new ComentarioModel { Comentario_id = id } );
            return ComentarioC.Message;
        }


        [Route("~/api/Comentario/{id}/GetComentarios")]
        [HttpGet]
        public async Task<MgkMessage> GetComentarios(int id)
        {

            ComentarioCtrl ComentarioC = new ComentarioCtrl();
            ComentarioC.Message.OData = ComentarioC.GetAll(new ComentarioModel { Comentario_id_padre = id } );
            return ComentarioC.Message;
        }


        // POST: api/Comentario
        public async Task<MgkMessage> Post([FromBody]ComentarioModel value)
        {
            ComentarioCtrl ComentarioC = new ComentarioCtrl();
            return ComentarioC.Insert(value );
        }

        [Route("~/api/Comentario/Creatabla")]
        [HttpPost]
        public async Task<MgkMessage> CrearTabla()
        {
            ComentarioCtrl ComentarioC = new ComentarioCtrl();
            return ComentarioC.CrearTabla();
        }

        [Route("~/api/Comentario/Buscar")]
        [HttpPost]
        public async Task<MgkMessage> PostBuscar([FromBody]ComentarioModel value)
        {
            ComentarioCtrl ComentarioC = new ComentarioCtrl();
            var Items = ComentarioC.GetAll(value );
            ComentarioC.Message.OData = Items;
            return ComentarioC.Message;
        }


        // PUT: api/Comentario/5
        public async Task<MgkMessage> Put(int id, [FromBody]ComentarioModel value)
        {
            ComentarioCtrl ComentarioC = new ComentarioCtrl();
            value.Comentario_id = id;
            return ComentarioC.Update(value );
        }

        // Delete: api/Comentario/5
        public async Task<MgkMessage> Delete(int id)
        {
            ComentarioCtrl ComentarioC = new ComentarioCtrl();
            return ComentarioC.Delete(new ComentarioModel { Comentario_id = id } );
        }
    }
}