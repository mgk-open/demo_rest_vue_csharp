﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mgk.Demo.Resvue.Base.Ticket
{
    public class TicketDao
    {
        List<TicketModel> Tickets;
        String FileName = AppDomain.CurrentDomain.BaseDirectory+"JsonTickets.txt";
        int Ticket_id_max = 1;

        /// <summary>
        /// Cargar lista de tickets desde archivo json
        /// </summary>
        private void LeerArchivo()
        {
            try
            {
                String TxtJson = File.ReadAllText(FileName, Encoding.UTF8);
                Tickets = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TicketModel>>(TxtJson);
                if (Tickets!=null)
                    Ticket_id_max=Tickets.OrderByDescending(t => t.Ticket_id).First().Ticket_id+1;
            }
            catch(Exception e)
            {
                Tickets = null;
                Console.WriteLine(e.ToString());
            }
        }


        private int GrabarArchivo()
        {
            if (Tickets != null)
            {
                try
                {
                    String TxtJson = Newtonsoft.Json.JsonConvert.SerializeObject(Tickets);
                    System.IO.File.WriteAllText(FileName, TxtJson);
                    return 0;
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return -2;
                }
            }
            return -1;
        }


        public TicketModel Agregar(TicketModel TicketM)
        {
            LeerArchivo();
            TicketM.Ticket_id = Ticket_id_max;
            if (Tickets == null)
                Tickets = new List<TicketModel>();
            Tickets.Add(TicketM);
            GrabarArchivo();
            return TicketM;
        }

        public int Eliminar(TicketModel TicketM)
        {
            LeerArchivo();
            if (Tickets.Exists(t => t.Ticket_id == TicketM.Ticket_id))
            {
                Tickets.RemoveAll(t => t.Ticket_id == TicketM.Ticket_id);
                return GrabarArchivo();
            }
            return -2;
        }

        public List<TicketModel> Listado()
        {
            LeerArchivo();
            return Tickets;
        }

        public TicketModel Modificar(TicketModel TicketM)
        {
            int Index = Tickets.FindIndex(t => t.Ticket_id == TicketM.Ticket_id);
            if (Index >= 0)
                Tickets[Index] = TicketM;
            else
                TicketM.Ticket_id *= -1;
            return TicketM;
        }

        public TicketModel Leer(TicketModel TicketM)
        {
            int Index = Tickets.FindIndex(t => t.Ticket_id == TicketM.Ticket_id);
            if (Index >= 0)
                return Tickets[Index];
            return null;
        }

    }
}
