﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mgk.Demo.Resvue.Base.Ticket
{
    public class TicketModel
    {
        public int Ticket_id { get; set; }
        public String Asunto { get; set; }
        public String Descripcion { get; set; }
        public Boolean Es_abierto{ get; set; }
    }
}
