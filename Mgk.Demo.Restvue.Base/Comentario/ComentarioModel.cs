﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mgk.Demo.Restvue.Base.Comentario
{
    public class ComentarioModel
    {
        public static string __table_name { get; } = "t_comentario";
        public static string[] __table_keys { get; } = { "Comentario_id" };
        public static bool __auto_increment { get; } = true;

        public int Comentario_id { get; set; }
        public DateTime Fecha { get; set; }
        public String Comentario { get; set; }
        public String Autor { get; set; }
        public int Comentario_id_padre { get; set; }

        public DateTime? _Fecha_desde { get; set; }
        public DateTime? _Fecha_hasta{ get; set; }
    }

//   CREATE TABLE t_comentario(
//       Comentario_id int NOT NULL IDENTITY primary key,
//       Comentario_id_padre int ,
//       Fecha datetime NULL,
//       Comentario varchar(200) NULL,
//      Autor varchar(20) NULL); 

}