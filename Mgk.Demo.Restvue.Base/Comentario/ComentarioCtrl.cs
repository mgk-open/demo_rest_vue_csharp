﻿using Mgk.Commonsx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mgk.Demo.Restvue.Base.Comentario
{
    public class ComentarioCtrl
    {
        private ComentarioDao ComentarioD { get; set; }

        public MgkMessage Message { get; set; }

        public ComentarioCtrl()
        {
            Message = new MgkMessage();
            ComentarioD = new ComentarioDao();
        }

        /// <summary>
        /// Antes de guardar veririfica si existe para hacer update o se trata de un registro nuevo
        /// </summary>
        /// <param name="ComentarioMod"></param>
        /// <param name="AccesoM"></param>
        /// <returns></returns>
        public MgkMessage Save(ComentarioModel ComentarioMod)
        {
            ComentarioModel ComentarioModNew = (ComentarioModel)ComentarioD.ReadObject(ComentarioMod, null, true);
            if (ComentarioModNew != null && ComentarioModNew.Comentario_id != 0)
                return this.Update(ComentarioMod);
            else
                return this.Insert(ComentarioMod);
        }

        /// <summary>
        /// Insertar Comentario
        /// </summary>
        /// <param name="ComentarioM"></param>
        /// <returns></returns>
        public MgkMessage Insert(ComentarioModel ComentarioM)
        {
            Message = ComentarioD.Insert(ComentarioM);

            return Message;
        }

        public MgkMessage CrearTabla()
        {
            Message = ComentarioD.CrearTabla();
            return Message;
        }

        /// <summary>
        /// Actualizar Comentario
        /// </summary>
        /// <param name="ComentarioM"></param>
        /// <returns></returns>
        public MgkMessage Update(ComentarioModel ComentarioM)
        {
            Message = ComentarioD.Update(ComentarioM);
            return Message;
        }

        /// <summary>
        /// Eliminar Comentario
        /// </summary>
        /// <param name="ComentarioM"></param>
        /// <param name="ComentarioM"></param>
        /// <returns></returns>
        public MgkMessage Delete(ComentarioModel ComentarioM)
        {
            Message = ComentarioD.Delete(ComentarioM);

            return Message;
        }


        public MgkMessage Get(ComentarioModel ComentarioM)
        {
            ComentarioModel ComentarioItem = ComentarioD.GetItem(ComentarioM);
            Message = ComentarioD.Message;
            Message.OData = ComentarioItem;
            return Message;
        }


        /// <summary>
        /// Leer Lista de Comentarios
        /// </summary>
        /// <param name="ComentarioM"></param>
        /// <param name="ComentarioM"></param>
        /// <returns></returns>
        public List<object> GetAll(ComentarioModel ComentarioM)
        {
            var ComentarioItems = ComentarioD.GetItems(ComentarioM);
            Message = ComentarioD.Message;

            return ComentarioItems;
        }

        public List<object> GetAll()
        {
            var ComentarioItems = ComentarioD.GetItems(new ComentarioModel());
            Message = ComentarioD.Message;

            return ComentarioItems;
        }
    }
}