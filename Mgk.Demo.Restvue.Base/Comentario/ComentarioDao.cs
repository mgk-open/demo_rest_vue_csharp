﻿using Mgk.Commonsx;
using Mgk.DataBasex;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mgk.Demo.Restvue.Base.Comentario
{
    public class ComentarioDao : MgkDataBaseObj
    {
        public MgkMessage Message { get; set; }

        public ComentarioDao()
        {
            Message = new MgkMessage();
        }



        public MgkMessage Insert(ComentarioModel ComentarioMo)
        {
            Message.Clear();
            try
            {
                ComentarioMo.Fecha = DateTime.Now;                
                this.InsertObject(ComentarioMo);
                int iRetVal = ComentarioMo.Comentario_id;

                if (iRetVal > 0)
                {
                    ComentarioMo.Comentario_id = iRetVal;
                    Message.Number = ComentarioMo.Comentario_id;
                    Message.Message = "Registro Insertado exitosamente";
                    Message.OData = ComentarioMo;
                }
                else
                {
                    throw new Exception("No fue posible Insertar el registro");
                }
            }
            catch (Exception ex)
            {
                Message = new MgkMessage
                {
                    Number = -1,
                    Code = "ex-ins",
                    Message = "Error al Insertar registro",
                    Source = this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    Exception = ex.ToString(),
                    OData = new { ComentarioMo = ComentarioMo }
                };
                MgkLog.Error(Message);
            }
            this.ConnectionClose();
            return Message;
        }

        public MgkMessage Update(ComentarioModel ComentarioMo)
        {
            Message.Clear();
            try
            {
                this.UpdateObject(ComentarioMo);
                Message = this.Messages.GetLastMessage();
            }
            catch (Exception ex)
            {
                Message = new MgkMessage
                {
                    Number = -2,
                    Code = "ex-upd",
                    Message = "Error al actualizar registro",
                    Source = this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    Exception = ex.ToString(),
                    OData = new { ComentarioMo = ComentarioMo }
                };
                MgkLog.Error(Message);
            }
            this.ConnectionClose();
            return Message;
        }


        public MgkMessage Delete(ComentarioModel ComentarioMo)
        {
            Message.Clear();
            try
            {
                this.DeleteObject(ComentarioMo);
                Message = this.Messages.GetLastMessage();
            }
            catch (Exception ex)
            {
                Message = new MgkMessage
                {
                    Number = -3,
                    Code = "ex-upd",
                    Message = "Error al actualizar registro",
                    Source = this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    Exception = ex.ToString(),
                    OData = new { ComentarioMo = ComentarioMo }
                };
                MgkLog.Error(Message);
            }
            this.ConnectionClose();
            return Message;
        }


        public ComentarioModel GetItem(ComentarioModel ComentarioMo)
        {
            Message.Clear();
            ComentarioModel ComentarioItem = null;
            try
            {
                ComentarioItem = (ComentarioModel)this.ReadObject(ComentarioMo, null, true);

            }
            catch (Exception ex)
            {
                Message = new MgkMessage
                {
                    Number = -4,
                    Code = "ex-sel4",
                    Message = "Error en consulta del registro",
                    Source = this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    Exception = ex.ToString(),
                };
                MgkLog.Error(Message);
            }
            this.ConnectionClose();
            return ComentarioItem;
        }

        public List<object> GetItems(ComentarioModel ComentarioMo)
        {
            Message.Clear();
            MgkQueryBuilder QueryB = new MgkQueryBuilder();

            try
            {
                #region Paso de Parametros
                List<DbParameter> Parameters = new List<DbParameter>();
                QueryB.SetQueryBase("select * from "+ ComentarioModel.__table_name);
                if (ComentarioMo.Comentario_id > 0)
                {
                    Parameters.Add((DbParameter)GetParameter("@Comentario_id", ComentarioMo.Comentario_id));
                    QueryB.AddAnd("Comentario_id=@Comentario_id");
                }

                if (ComentarioMo.Comentario_id_padre > 0)
                {
                    Parameters.Add((DbParameter)GetParameter("@Comentario_id_padre", ComentarioMo.Comentario_id_padre));
                    QueryB.AddAnd("Comentario_id_padre=@Comentario_id_padre");
                }

                if (ComentarioMo._Fecha_desde!=null)
                {
                    Parameters.Add((DbParameter)GetParameter("@Fecha_desde", ComentarioMo._Fecha_desde));
                    Parameters.Add((DbParameter)GetParameter("@Fecha_hasta", ComentarioMo._Fecha_hasta));
                    QueryB.AddAnd("Fecha>=@Fecha_desde");
                }

                if (ComentarioMo._Fecha_hasta != null)
                {
                    Parameters.Add((DbParameter)GetParameter("@Fecha_hasta", ComentarioMo._Fecha_hasta));
                    QueryB.AddAnd("Fecha <=@Fecha_hasta");
                }

                if (ComentarioMo.Autor != null)
                {
                    Parameters.Add((DbParameter)GetParameter("@Autor", ComentarioMo.Autor));
                    QueryB.AddAnd("Fecha like '%@Autor%'");
                }


                #endregion

                var Itemsd = this.ReadODictionaryListByQuery(ComentarioMo, QueryB.GetQuery(), Parameters);
                Message.Number = Itemsd.Count;
                this.ConnectionClose();
                return Itemsd;
            }
            catch (Exception ex)
            {
                Message = new MgkMessage
                {
                    Number = -5,
                    Code = "ex-sel5",
                    Message = "Error en consulta de lista",
                    Source = this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name,
                    Exception = ex.ToString(),
                    OData = new { ComentarioMo = ComentarioMo, QueryB = QueryB }
                };
                MgkLog.Error(Message);
            }
            this.ConnectionClose();
            Message.Number = 0;
            return null;
        }


        /// <summary>
        /// Script para crear tabla. aun no es dinamico basado en una clase
        /// </summary>
        /// <returns></returns>
        public MgkMessage CrearTabla()
        {
            Message.Clear();
            try
            {
                this.GetConnection();

                StringBuilder Query = new StringBuilder();
                Query.Append(" create table t_comentario(");
                Query.Append(" Comentario_id ");
                if (this.DataBaseEngine == DataBaseEngineEnum.MySqlClient)
                    Query.Append(" int not null auto_increment, ");
                if (this.DataBaseEngine == DataBaseEngineEnum.SqlClient)
                    Query.Append(" int not null identity, ");
                if (this.DataBaseEngine == DataBaseEngineEnum.Npgsql)
                    Query.Append(" serial, ");
                Query.Append(" Comentario_id_padre int not null,");
                Query.Append(" Fecha ");
                if (this.DataBaseEngine == DataBaseEngineEnum.MySqlClient)
                    Query.Append(" timestamp, ");
                if (this.DataBaseEngine == DataBaseEngineEnum.SqlClient)
                    Query.Append(" Datetime, ");
                if (this.DataBaseEngine == DataBaseEngineEnum.Npgsql)
                    Query.Append(" serial, ");
                Query.Append(" timestamp,");
                Query.Append(" Comentario varchar(200),");
                Query.Append(" Autor varchar(20),");
                Query.Append(" constraint t_comentario_pk primary key(comentario_id) );");
                
                Message.Number = this.ExecuteNonQuery(Query.ToString(),null, CommandType.Text);
                Message.Message = "Parece que la Tabla fue creada exitosamente";
            }
            catch (Exception ex)
            {
                Message.Number = -1;
                Message.Message = "Excepcion al crea Tabla";
                Message.Exception = ex.ToString();
            }

            return Message;
        }

    }
}